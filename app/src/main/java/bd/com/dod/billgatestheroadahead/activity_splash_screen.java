package bd.com.dod.billgatestheroadahead;


        import android.content.Intent;
        import android.net.Uri;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.TextView;

public class activity_splash_screen extends AppCompatActivity {
    Button started;
    ImageView app1,app2,app3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ImageView imageView = (ImageView)findViewById(R.id.rotate);
        TextView textView=(TextView)findViewById(R.id.text);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotateanim);
        imageView.startAnimation(animation);

        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.downanim);
        textView.startAnimation(animation1);

        //started
        started = (Button)findViewById(R.id.started);
        started.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                Intent i = new Intent(activity_splash_screen.this, MainActivity.class);
                startActivity(i);

            }

        });

        //app1
        app1 = (ImageView)findViewById(R.id.app1);
        app1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=bd.com.dod.usatimes")); // Add package name of your application
                startActivity(intent);

            }

        });
        //app2
        app2 = (ImageView)findViewById(R.id.app2);
        app2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=bd.com.dod.billgateslife")); // Add package name of your application
                startActivity(intent);

            }

        });
        //app3
        app3 = (ImageView)findViewById(R.id.app3);
        app3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/dev?id=4774425052046495784")); // Add package name of your application
                startActivity(intent);

            }

        });


    }
}

